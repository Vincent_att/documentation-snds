# Comment accéder au SNDS ?
<!-- SPDX-License-Identifier: MPL-2.0 -->

En dehors de l'Open Data, présenté plus haut, deux types distincts d'accès aux données du SNDS sont possibles : les accès permanents, et les accès sur projet.

## Les accès permanents :

Certains organismes chargés d'une mission de service public, [listés par décret](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033702840&categorieLien=id), disposent d'un accès permanent aux données du SNDS.
Cette autorisation pérenne concerne un périmètre spécifique des données, et se décline ainsi selon les niveaux d'agrégation (données individuelles/agrégées bénéficiaires, …) et les historiques accessibles.

## Les accès sur projet :

Pour tous les demandeurs, publics comme privés, la loi prévoit la possibilité de demander un accès à une extraction de données du SNDS pour un projet d'intérêt public à des fins de recherche, d'étude et d'évaluation dans le domaine de la santé.
Selon les cas, cette demande d'accès peut relever de différentes procédures.

### La procédure standard :

La procédure standard d'accès au SNDS, la plus courante, suit les étapes suivantes :

1. Le Responsable de Traitement (RT) dépose une demande d'autorisation auprès de l'Institut National des Données de Santé (INDS), qui valide la complétude du dossier, et peut s'auto-saisir pour examiner l'intérêt public du traitement (délai : 7 jours)
2. Le Comité d'Expertise pour les Recherches, les Etudes et les Evaluations dans le domaine de la Santé (CEREES) émet un avis sur la méthodologie scientifique du projet, et le recours à des données à caractère personnel (délai : 1 mois)
3. La Commission Nationale de l'Informatique et des Libertés ([CNIL](../glossaire/CNIL.md)) prend connaissance des avis du CEREES et de l'INDS, et donne une autorisation pour le projet (délai : 2 mois, renouvelables une fois)
4. En fonction de l'autorisation [CNIL](../glossaire/CNIL.md), une convention est signée entre la [CNAM](../glossaire/Cnam.md) et le RT, afin que la [CNAM](../glossaire/Cnam.md) puisse extraire les données SNDS sur le périmètre autorisé (délai indicatif : 2 mois)

### Les méthodologies de référence :

Les méthodologies de référence (MR) sont des procédures simplifiées d'accès aux données, qui permettent dans certaines situations de réaliser une recherche en santé sans nécessiter d'autorisation [CNIL](../glossaire/CNIL.md) ou d'avis du CEREES.
Le Responsable de Traitement adresse alors à la [CNIL](../glossaire/CNIL.md) une déclaration attestant la conformité du projet à la MR, puis inscrit son traitement dans le [répertoire public tenu par l'INDS](https://www.indsante.fr/fr/repertoire-public-des-etudes-realisees-sous-mr).

### La procédure simplifiée d'accès à l'[EGB](../glossaire/EGB.md) :

Début 2019, la [CNIL](../glossaire/CNIL.md) a homologué une procédure d'accès simplifiée à l'[EGB](../glossaire/EGB.md), en sa qualité d'échantillon représentatif.
La [CNIL](../glossaire/CNIL.md) donne ainsi compétence à l'INDS pour approuver (délai : 15 jours) l'accès à l'[EGB](../glossaire/EGB.md) après examen des 5 conditions suivantes :

- la finalité d'intérêt public du projet
- la justification par le RT de la pertinence scientifique du projet
- l'absence de croisements d'identifiants potentiels
- la durée d'accès aux données qui doit être limitée à celle nécessaire à la réalisation du projet
- le respect du référentiel de sécurité SNDS

### Les assureurs et industriels de santé :

L'accès des industriels de santé (entreprises productrices de produits de santé) et des assureurs en santé au SNDS est plus fortement encadré.
Ces acteurs doivent :

- soit passer par un bureau d'études ou un organisme de recherche indépendant
- soit démontrer l'impossibilité d'utiliser le SNDS pour des finalités interdites