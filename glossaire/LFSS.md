# LFSS - Loi de Financement de la Sécurité Sociale
<!-- SPDX-License-Identifier: MPL-2.0 -->

La Loi de Financement de la Sécurité Sociale (LFSS) est votée chaque année par le Parlement ; elle fixe l’objectif national des dépenses d’assurance maladie ([Ondam](ONDAM.md)).

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Loi_de_financement_de_la_S%C3%A9curit%C3%A9_sociale)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/lfss-loi-de-financement-de-la-securite-sociale) sur le site internet du ministère
