# RIM-P : Recueil d'Information Médicalisé pour la Psychiatrie
<!-- SPDX-License-Identifier: MPL-2.0 -->

Information médicale en établissement hospitalier de psychiatrie

[Présentation RIM-P](https://www.atih.sante.fr/psy/presentation)