# ALD - Affection de Longue Durée
<!-- SPDX-License-Identifier: MPL-2.0 -->

Attention, les dates de fin d'ALD ne sont pas toujours renseignées (lorsque ALD toujours en cours)
