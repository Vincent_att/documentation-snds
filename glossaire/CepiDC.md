# CépiDC - Centre d'épidémiologie sur les causes médicales de décès
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le Centre d'épidémiologie sur les causes médicales de Décès (CépiDc) est une unité de service en charge de cette mission pour l'Inserm depuis 1968. Il dirige le Centre Collaborateur OMS (CCOMS) sur les classifications internationales en santé en langue française.

